var express = require('express');
const path = require('path');
var router = express.Router();

// const app = express();

// Serve static files from the React app
router.use(express.static(path.join(__dirname, 'client/build')));

/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'Express' });
// });
router.get('*', (req, res) => {
  res.sendFile(path.join(__dirname+'/client/build/index.html'));
});

module.exports = router;